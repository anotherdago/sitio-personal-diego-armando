$(function(){

	var animaciones = function(){
		$("#logo").animate({opacity:1}, 4000);
		$("h1,h2").delay(700).animate({opacity:1}, 4000);

		var t = 1000;
		$("li").each(function(){
			$(this).delay(2000).animate({"opacity":1},t);
			t = t + 600;
		});

		$('.ico-social').mouseenter(function(){

			$(this).animate({"left": "25px", "width": "32px","height": "32px"}, 600);
			$(this).next(".button").animate({"opacity": "1"}, "medium");
		});

		$('.button').mouseleave(function(){
			$('.ico-social').animate({"left": "100px", "width": "56px","height": "56px"}, 600);
			$(this).animate({"opacity": "0"}, "medium");
		});
	};

	animaciones();
		
});

